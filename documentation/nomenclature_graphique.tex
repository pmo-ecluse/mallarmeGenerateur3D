\documentclass[a4paper,french]{article}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage[weather]{ifsym}
\usepackage[pdftex]{graphicx}
\usepackage[acronym]{glossaries}
\makeglossaries
\usepackage{ifthen}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,arrows.meta}
\usepackage{lcg}
\usepackage{varioref}
\usepackage[bottom=2.5cm,top=3cm]{geometry}
\usepackage[%
	pdftex,%
	pdfpagelabels=true,%
	pdftitle={Nomenclature graphique},%
	pdfauthor={Grégory DAVID \& Abdellatif BAYBAY},%
        colorlinks,%
]{hyperref}

\author{Grégory \textsc{David} \& Abdelatif \textsc{Baybay}}
\title{Nomenclature graphique}
\date{\today}

\newacronym{flo}{FL}{Florence \textsc{Loison}}
\newacronym{anna}{AT}{Annabelle \textsc{Tison}}

\newcommand{\lesdeux}{\gls{anna} \& \gls{flo}}

\newcommand{\figureTikz}[2]{
  \begin{figure}[h!]
      \centering
      \IfFileExists{#2.tex}
      {
        \begin{tikzpicture}
            \def\distance{2mm};
            \def\annabelle{
              \draw [color=danseuse] (a) node {$\bullet$} node [below=\distance]{\pgfmathparse{random(1000000)}\pgfmathresult};
              \draw [color=qqccqq] (a) node [left]{\gls{anna}};
            };
            \def\florence{
              \draw [color=danseuse] (f) node {$\bullet$} node [above=\distance]{\pgfmathparse{random(1000000)}\pgfmathresult};
              \draw [color=qqccqq] (f) node[right] {\gls{flo}};
            };
            \input{#2.tex}
        \end{tikzpicture}
      }
      {\includegraphics[width=10cm]{#2}}
      \caption{#1}
      \ifthenelse{\equal{#2}{}}{}{\label{fig:#2}}
  \end{figure}
}

\newcommand{\Element}[7]{
\ifthenelse{\equal{#1}{}}{}{
  \subsection{#1}
  \begin{description}
      \item[Nom :] #1
      \item[Danseuse(s) impliquée(s) :] \ifthenelse{\equal{#2}{}}{N/A}{#2}
      \item[Nature du trait :] \ifthenelse{\equal{#3}{}}{N/A}{#3}
      \item[Commentaire :] \ifthenelse{\equal{#4}{}}{N/A}{#4}
      \item[Implémentation :] \ifthenelse{\equal{#6}{}}{\FilledRainCloud}{\Sun\newline{\scriptsize\url{https://github.com/PMO-Ecluse/mallarmeGenerateur3D/commit/#6}}}
      \item[Chemin(s) OSC :] \ifthenelse{\equal{#7}{}}{N/A}{~\newline{\ttfamily #7}}
      \ifthenelse{\equal{#5}{}}{}{\item[Schéma
        :] Voir \figurename{} \vref{fig:#5}.\newline
        \figureTikz{#1}{#5}
      }
  \end{description}
}
\newpage
}

\definecolor{qqccqq}{rgb}{0,0.8,0}
\definecolor{danseuse}{rgb}{0.5,0.5,0.5}

\begin{document}
\maketitle{}
\tableofcontents{}
\newpage

\section{Conventions utilisées dans ce document}
\begin{itemize}
    \item Tout tracé en niveau de gris sera projeté en blanc.
    \item Tout tracé en vert ne sera pas projeté et est un élément de
    compréhension de la figure.
    \item Toute information dessinée en rouge correspond à une
    contrainte.
\end{itemize}

\section{Éléments systématiques}
\begin{itemize}
    \item Cote danseuse : chaque danseuse est suivie par le
    \emph{temps universel}, situé sur un rayon de 1m autour d'elle,
    adapté en fonction de la position (proposition : faire tourner la
    valeur continuellement autour de la danseuse, comme une orbite).
    \item \textbf{Aucune information ne doit être projetée sur les
      danseuses}.
    \item Plan de projection : le plan de projection est de
    6m$\times$6m, l'origine du repère est centré, comme dans la
    \figurename{}\vref{fig:planProjection} (cependant, d'un point de
    vue informatique l'origine du repère est centré dans le coin
    supérieur gauche :
    Cf. \figurename{}\vref{fig:planProjectionNumerique}).
    \item Le \textbf{diamètre d'un point} dessiné au sol doit être
    \textbf{égal à 3cm}.
    \item La \textbf{distance} entre chaque point des lignes
    pointillées est de \textbf{20cm au sol}.
    \item Les \textbf{lettres}, \textbf{chiffres} et \textbf{symboles}
    sont de taille \textbf{30cm au sol}.
    \item Les danseuse \gls{anna} et \gls{flo} sont représentées par
    leurs acronymes.
\end{itemize}

\figureTikz{Schématisation du plan de projection, avec son repère
  orthonormé et centré}{planProjection} \figureTikz{Schématisation du
  plan de projection, avec son repère orthonormé et centré dans le
  coin supérieur gauche}{planProjectionNumerique}

\Element{Gestion des
  danseuses}{\lesdeux{}}{}{}{}{3ac9f4a}{/mallarme/(annabelle|florence)/position
  ii [0..1024] [0..768]\newline/mallarme/(annabelle|florence)/position
  ff [0..1] [0..1]}

\section{Scène entrée}
\Element{Nuit étoilée}{\lesdeux{}}{}{A/ apparition d'un point en même
  temps que les danseuses, B/ les autres points apparraissent cadencés
  progressivement : 30s, 20s, 10s, 5s, 1s, C/ Les constellations sont
  tracées, D/ le passage des danseuses au travers des étoiles fait
  bouger les étoiles tout en conservant les liens de constellation
  (box2D)}{nuitEtoilee}{}

\section{Mesures}
\Element{Pointillés}{\lesdeux{}}{}{chaque danseuse possède une lettre
  ou un nombre l'accompagnant : la cote
  danseuse}{pointilles}{aff6b493a2844666c194c36d8cfd70c06fd1cb0a}{/mallarme/geometriesSimples/pointilles [TF]\newline/mallarme/geometriesSimples/pointilles [if] [01]}

\Element{Distance entre deux points}{\lesdeux{}}{}{la distance entre
  les danseuses est mesurée et projetée au plus près du public, selon
  l'horizontale}{distanceEntreDeuxPoints}{aff6b493a2844666c194c36d8cfd70c06fd1cb0a}{/mallarme/geometriesSimples/distance [TF]\newline/mallarme/geometriesSimples/distance [if] [01]}

\Element{Sinusoïde élastique}{\lesdeux{}}{}{A/ tracé simple de la
  courbe, B/ ajouter les coordonnées en ordonnée sur l'axe des
  abscisses}{sinusoideElastiqueCoordonnees}{5644de2f95e68aff285b195b5a6bc2182abc9c20}{/mallarme/geometriesSimples/sinusElastique
  [TF]\newline/mallarme/geometriesSimples/sinusElastique [if] [01]}
 
\Element{Sinusoïde à amplitude et phase
  constantes}{}{}{}{sinusoide}{75b2556249b4e6a734164a02570b8c1a6e4d9b64}{/mallarme/geometriesSimples/sinusConstante
  [TF]\newline/mallarme/geometriesSimples/sinusConstante [if] [01]}

\Element{Courbes à variations}{\lesdeux{}}{}{plusieurs variations : A/
  Bezier entre les danseuses, B/ cercle entre danseuses, C/ triangle
  inscrit dans le cercle. À chaque nouvelle variation, l'ancienne
  s'estompe en
  pointillés}{courbesAVariations}{}{/mallarme/geometriesSimples/courbesVariations
  [TF]\newline/mallarme/geometriesSimples/courbesVariations [if] [01]}

\Element{Cercle de diamètre danseuses}{\lesdeux{}}{}{pas de mesure du
  diamètre à
  tracer}{cercleDiametreDanseuses}{38404c39d7c0aecbce0c6b33c0109a9c955cc160}{/mallarme/geometriesSimples/cercleDiametre
  [TF]\newline/mallarme/geometriesSimples/cercleDiametre [if] [01]}

\Element{Double ellipse}{\lesdeux{}}{}{deux ellipses imbriquées. l'une
  est tracée depuis l'extérieur de
  l'affichage}{doubleEllipse}{74138a44b534e252395401148d55f69459daa458}{/mallarme/geometriesSimples/doubleEllipse
  [TF]\newline/mallarme/geometriesSimples/doubleEllipse [if] [01]}

\Element{Double cercle}{\lesdeux{}}{}{chaque danseuse est inscrite
  dans un cercle de diamètre
  constant}{doubleCercle}{8a235fd6c1559cb765e519a81a76979749976b8b}{/mallarme/geometriesSimples/doubleCercle
  [TF]\newline/mallarme/geometriesSimples/doubleCercle [if] [01]}

\Element{Double inscrit}{\lesdeux{}}{}{l'une est inscrite dans un
  cercle proche d'elle, les deux sont inscrites dans un grand
  cercle}{doubleInscrit}{43191798f4459be773edccfc42e8584115e43525}{/mallarme/geometriesSimples/doubleInscrit
  [TF]\newline/mallarme/geometriesSimples/doubleInscrit [if] [01]}

\Element{Intersection du double cercle}{\lesdeux{}}{}{on ne garde que
  l'intersection des deux cercles à diamètre
  constant}{intersectionDoubleCercle}{b35f7679c5b22e8ab8b8228550562ad9a99e21d9}{/mallarme/geometriesSimples/intersectionDoubleCercle
  [TF]\newline/mallarme/geometriesSimples/intersectionDoubleCercle [if] [01]}

\Element{Cardinalités}{\lesdeux{}}{}{basé sur 5 valeurs : constante
  'a', temps universel, angle, posX,
  posY}{cardinalites}{3671e56714206aaf645c0798a3bfbe524f080b52}{/mallarme/geometriesSimples/cardinalites
  [TF]\newline/mallarme/geometriesSimples/cardinalites [if] [01]}

\Element{Coordonnées des danseuses}{\lesdeux{}}{}{chacune a son propre
  repère : A/ bottom/left, B/
  top/right}{coordonneesDanseuses}{4051e6b3a0ddc8cbe0faf246c7f0d17bfe2686a5}{/mallarme/geometriesSimples/coordonnees
  [TF]\newline/mallarme/geometriesSimples/coordonnees [if] [01]}

\section{Escaliers}
\Element{Xenakis droite}{\lesdeux{}}{}{tracé des droites
  perpendiculaires à la
  trajectoire}{xenakisDroite}{596da18ab4acdab17bf6d7bcb92a735d7f505707}{/mallarme/xenakis/droite [TF]\newline/mallarme/xenakis/droite [if] [01]}

\Element{Xenakis baton}{\lesdeux{}}{}{tracé du baton perpendiculaire à
  la trajectoire, les premiers tracés disparaissent à mesure que de
  nouveaux tracés apparaissent : on affiche toujours 15
  tracés}{xenakisBaton}{596da18ab4acdab17bf6d7bcb92a735d7f505707}{/mallarme/xenakis/baton [TF]\newline/mallarme/xenakis/baton [if] [01]}

\Element{Xenakis data}{\lesdeux{}}{}{affichage de l'abscisse sur les
  points, extrémités des
  batons}{xenakisData}{596da18ab4acdab17bf6d7bcb92a735d7f505707}{/mallarme/xenakis/data [TF]\newline/mallarme/xenakis/data [if] [01]}

\Element{Xenakis cercle}{\lesdeux{}}{}{entre chaque capture de
  position, tracer le cercle de rayon la distance entre la capture
  précédente la
  présente}{xenakisCercle}{596da18ab4acdab17bf6d7bcb92a735d7f505707}{/mallarme/xenakis/cercle [TF]\newline/mallarme/xenakis/cercle [if] [01]}

\section{Géométries compliquées}
\Element{Complexe 001}{}{}{la figure complète est tracée de façon
  ordonnée, objet par objet, en fonction de la position des
  danseuses. à la fin du tracé, la figure est effacée et redessinée de
  la même façon, mais avec un ordre aléatoirement
  redistribué}{complexe001}{}{}



% \Element{}{}{}{}{}


\printglossaries{}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "nomenclature_graphique"
%%% End: 
