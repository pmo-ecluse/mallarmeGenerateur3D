#pragma once

#include "Element.h"
#include "Xenakis.h"

class XenakisDroite :
    public Xenakis
{
public:
    XenakisDroite(string nom, char key, ofVec2f * _pointOrigine);
    virtual ~XenakisDroite();
    void draw();
};
