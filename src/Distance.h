#pragma once

#include "Element.h"
#include "Vecteur.h"

class Distance :
    public Element
{
public:
    Distance(string nom, char key, int _font = 13);
    Distance(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~Distance();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();

    void fontSize(int _size)
    {
        _fontSize = _size;
        _ttfText->loadFont("/usr/share/fonts/truetype/dejavu/DejaVuSans-Oblique.ttf", _fontSize, true, true);
    };
    int fontSize()
    {
        return _fontSize;
    };

    Vecteur * vecteur()
    {
        return _vecteur;
    };

private:
    ofTrueTypeFont * _ttfText;
    int _fontSize;
    Vecteur * _vecteur;
};
