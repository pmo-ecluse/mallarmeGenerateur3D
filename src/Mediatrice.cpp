#include "Mediatrice.h"

Mediatrice::Mediatrice(string nom, char key) :
    Perpendiculaire(nom, key)
{
	positionRelative = 0.5f;
}

Mediatrice::Mediatrice(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    Mediatrice(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

Mediatrice::~Mediatrice()
{
}
