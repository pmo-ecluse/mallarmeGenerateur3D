#include "CourbeBezier.h"
#include "Vecteur.h"

CourbeBezier::CourbeBezier(string nom, char key, ofVec2f * _pointControlA, ofVec2f * _pointControlB) :
    Element(nom, key),
    pointControlA(_pointControlA),
    pointControlB(_pointControlB),
    _showVecteur(false)
{
    initialiseOscMethodes();
}

CourbeBezier::CourbeBezier(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointControlA, ofVec2f * _pointControlB, ofVec2f * _pointDestination) :
    CourbeBezier(nom, key, _pointControlA, _pointControlB)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

CourbeBezier::~CourbeBezier()
{
}

void CourbeBezier::initialiseOscMethodes()
{
    addOscMethod("showVecteur");
}

void CourbeBezier::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "showVecteur"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            showVecteur(getArgAsBoolUnchecked(m, 0));
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void CourbeBezier::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        ofSetCurveResolution(40);
        ofBezier(Origine()->x, Origine()->y,
                 pointControlA->x, pointControlA->y,
                 pointControlB->x, pointControlB->y,
                 Destination()->x, Destination()->y);

        if(showVecteur())
        {
            Vecteur v(ofxOscRouterNode::getFirstOscNodeAlias() + "-vecteur", '@',
                      Origine(), pointControlA);
			v.Dessinable(Dessinable());
            v.lineWidth(lineWidth());
            v.Alpha(Alpha());
            v.draw();
            v.Origine(Destination());
            v.Destination(pointControlB);
            v.draw();
        }

        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

