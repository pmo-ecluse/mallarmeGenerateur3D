#include "XenakisDroite.h"

XenakisDroite::XenakisDroite(string nom, char key, ofVec2f * _pointOrigine) :
    Xenakis(nom, key, _pointOrigine)
{
}

XenakisDroite::~XenakisDroite()
{
}

void XenakisDroite::draw()
{
    if(Dessinable())
    {
        if(!xenakisQueue.empty())
        {
            ofPushStyle();
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, Alpha() * 255);
            ofSetLineWidth(lineWidth());
            ofVec2f previousPoint(xenakisQueue.front());
            float droiteSize(ofGetHeight() * 2.0f);

            for(itXenakisQueue = xenakisQueue.begin(); itXenakisQueue != xenakisQueue.end(); itXenakisQueue++)
            {
                ofPushMatrix();
                ofTranslate(*itXenakisQueue);
                ofRotateZ(Tools::getAngle(previousPoint, *itXenakisQueue));
                ofLine(0, -droiteSize, 0, droiteSize);
                ofPopMatrix();
                previousPoint.set(*itXenakisQueue);
            }

            ofPushMatrix();
            ofTranslate(*Origine());
            ofRotateZ(Tools::getAngle(previousPoint, *Origine()));
            ofLine(0, -droiteSize, 0, droiteSize);
            ofPopMatrix();
            ofDisableAlphaBlending();
            ofPopStyle();
        }
    }
}

