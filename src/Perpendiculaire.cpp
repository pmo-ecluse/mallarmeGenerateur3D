#include "Perpendiculaire.h"
#include "SegmentDroite.h"

Perpendiculaire::Perpendiculaire(string nom, char key, float _position) :
    Element(nom, key),
    showSymbole(true),
    positionRelative(_position)
{
    initialiseOscMethodes();
}

Perpendiculaire::Perpendiculaire(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination, float _position) :
    Perpendiculaire(nom, key, _position)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

Perpendiculaire::~Perpendiculaire()
{
}

void Perpendiculaire::initialiseOscMethodes()
{
    addOscMethod("showSymbole");
    addOscMethod("positionRelative");
}

void Perpendiculaire::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "showSymbole"))
    {
        if(validateOscSignature("([TFif])", m))
        {
            showSymbole = getArgAsBoolUnchecked(m, 0);
        }
    }
    else if(isMatch(command, "positionRelative"))
    {
        if(validateOscSignature("([f])", m))
        {
            positionRelative = getArgAsFloatUnchecked(m, 0);
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void Perpendiculaire::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        float angle(Tools::getAngle(*Origine(), *Destination()));
        ofVec2f AB(Destination()->x - Origine()->x, Destination()->y - Origine()->y);
        ofPushMatrix();
        ofTranslate(*Origine() + positionRelative * AB);
        ofRotateZ(angle);

        if(showSymbole)
        {
            float tailleSymbole(ofClamp(10 * lineWidth(), 15, 50));
            ofLine(0, tailleSymbole, tailleSymbole, tailleSymbole);
            ofLine(tailleSymbole, 0, tailleSymbole, tailleSymbole);
        }

        ofVec2f from(0, -ofGetHeight() * 2);
        ofVec2f to(0, ofGetHeight() * 2);

        if(typeTrace() == TRACE_POINTILLES)
        {
            SegmentDroite pointilles(ofxOscRouterBaseNode::getFirstOscNodeAlias() + "-segment", '@', &from, &to, TRACE_POINTILLES);
            pointilles.Alpha(Alpha());
            pointilles.Dessinable(Dessinable());
            pointilles.lineWidth(lineWidth());
            pointilles.offset(offset());
            pointilles.draw();
        }
        else
        {
            ofLine(from, to);
        }

        ofPopMatrix();
        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

