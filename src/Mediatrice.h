#pragma once

#include "Perpendiculaire.h"

class Mediatrice :
    public Perpendiculaire
{
public:
    Mediatrice(string nom, char key);
    Mediatrice(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~Mediatrice();
};
