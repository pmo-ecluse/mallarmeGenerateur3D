#pragma once

#include "Element.h"
#include "Xenakis.h"

class XenakisData :
    public Xenakis
{
public:
    XenakisData(string nom, char key, ofVec2f * _pointOrigine, int _font = 13);
    virtual ~XenakisData();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();

    void fontSize(int _size)
    {
        _fontSize = _size;
        _ttfText->loadFont("/usr/share/fonts/truetype/dejavu/DejaVuSans-Oblique.ttf", _fontSize, true, true);
    };
    int fontSize()
    {
        return _fontSize;
    };

private:
    ofTrueTypeFont * _ttfText;
    int _fontSize;
};
