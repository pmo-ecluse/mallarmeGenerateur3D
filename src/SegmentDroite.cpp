#include "SegmentDroite.h"
#include "Tools.h"

SegmentDroite::SegmentDroite(string nom, char key, TypeTrace _type) :
    Element(nom, key)
{
    typeTrace(_type);
}

SegmentDroite::SegmentDroite(string nom,
                             char key,
                             ofVec2f * _pointOrigine,
                             ofVec2f * _pointDestination,
                             TypeTrace _type) :
    SegmentDroite(nom, key, _type)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

SegmentDroite::~SegmentDroite()
{
}

void SegmentDroite::draw()
{
    if(Dessinable())
    {
        if(typeTrace() == TRACE_POINTILLES)
        {
            float distance(Tools::getDistance(*Origine(), *Destination()));
            float angle(Tools::getAngle(*Origine(), *Destination()));
            ofVec2f _origine(0, 0);
            ofVec2f _destination(distance, 0);
            // -----
            ofPushMatrix();
            ofTranslate(*Origine());
            ofRotateZ(angle);
            ofPushStyle();
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, Alpha() * 255);
            ofFill();

            for(unsigned int stepX = 0; stepX < distance; stepX += offset())
            {
                ofRect(_origine + ofVec2f(stepX, 0),
                       lineWidth() * 2,
                       lineWidth());
            }

            ofDisableAlphaBlending();
            ofPopStyle();
            ofPopMatrix();
        }
        else
        {
            ofPushStyle();
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, Alpha() * 255);
            ofSetLineWidth(lineWidth());
            ofLine(*Origine(), *Destination());
            ofDisableAlphaBlending();
            ofPopStyle();
        }
    }
}

