#include "XenakisCercle.h"

XenakisCercle::XenakisCercle(string nom, char key, ofVec2f * _pointOrigine) :
    Xenakis(nom, key, _pointOrigine)
{
}

XenakisCercle::~XenakisCercle()
{
}

void XenakisCercle::draw()
{
    if(Dessinable())
    {
        if(!xenakisQueue.empty())
        {
            ofPushStyle();
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255, Alpha() * 255);
            ofSetLineWidth(lineWidth());
            ofVec2f previousPoint(xenakisQueue.front());

            for(itXenakisQueue = xenakisQueue.begin(); itXenakisQueue != xenakisQueue.end(); itXenakisQueue++)
            {
                ofCircle(*itXenakisQueue, previousPoint.distance(*itXenakisQueue));
                previousPoint.set(*itXenakisQueue);
            }

            ofCircle(*Origine(), previousPoint.distance(*Origine()));
            ofDisableAlphaBlending();
            ofPopStyle();
        }
    }
}

