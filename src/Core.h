#pragma once
#ifndef CORE_H
#define CORE_H

#include "ofMain.h"
#include "ofxSceneManager.h"
#include "ofxOsc.h"
#include "ofxOscRouter.h"

#include "Tools.h"
#include "Danseur.h"
#include "scenes.h"
#include "Masque.h"

class Core :
    public ofBaseApp,
    public ofxOscRouter
{
    enum Conduite { START = 0, PASSAGE_A_TRAVERS_CARRES };
public:
    Core():
        ofBaseApp(),
        ofxOscRouter("mallarme3D", 9876),
        bDebug(false),
        bBlackOut(false)
    {
        ofxOscRouterNode::addOscMethod("exit");
        ofxOscRouterNode::addOscMethod("debug");
        ofxOscRouterNode::addOscMethod("switchToScene");
        ofxOscRouterNode::addOscMethod("blackout");
    };
    void setup();
    void update();
    void draw();
    void exit();

    void processOscCommand(const string& command, const ofxOscMessage& m);

    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);

private:
    Tools * compositeur;
    ofxSceneManager * sceneManager;
    sceneElements * ptr_sceneElements;
    scenePassageATraversCarresBlanc * ptr_scenePassageATraversCarresBlanc;
    ofxOscMessage m;
    ofxOscSender oscSend;
    bool bDebug;
    bool bBlackOut;

    Masque * masqueCadrage;
};

#endif

