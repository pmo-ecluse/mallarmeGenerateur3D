#include "EllipseDouble.h"

EllipseDouble::EllipseDouble(string nom, char key) :
    Element(nom, key)
{
}

EllipseDouble::EllipseDouble(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    EllipseDouble(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

EllipseDouble::~EllipseDouble()
{
}

void EllipseDouble::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        float distance(Tools::getDistance(*Origine(), *Destination()));
        float angle(Tools::getAngle(*Origine(), *Destination()));
        ofVec2f M(Origine()->middled(*Destination()));
        ofCircle(M, distance / 1.5f);
        ofPushMatrix();
        ofTranslate(M);
        ofRotateZ(angle);
        ofEllipse(0, 0, distance * 1.5f, distance / 3.0f);
        ofPopMatrix();
        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

