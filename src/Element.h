#pragma once

#include "ofMain.h"
#include "ofxOscRouterNode.h"
#include "ofxAnimatableFloat.h"
#include "Tools.h"
#include "Danseur.h"

class Element :
    public ofxOscRouterNode
{
public:
    enum TypeTrace { TRACE_LIGNE = 0, TRACE_POINTILLES, TRACE_REMPLIR};
    Element(string nom, char key, ofVec2f * _pointOrigine = &Annabelle, ofVec2f * _pointDestination = &Florence, TypeTrace _type = TRACE_LIGNE);
    virtual ~Element();
    virtual void processOscCommand(const string& command, const ofxOscMessage& m);
    virtual void update(float);
    virtual void draw();
    virtual void apparait(float valDestination = 1);
    virtual void disparait();
    void Origine(ofVec2f * _origine);
    ofVec2f * Origine();
    void Destination(ofVec2f * _destination);
    ofVec2f * Destination();

    void Alpha(float _valAlpha)
    {
        _alpha.reset(ofClamp(_valAlpha, 0, 1));
    };
    float Alpha()
    {
        return _alpha.val();
    };

    void lineWidth(float width)
    {
        _lineWidth = width;
    };
    float lineWidth()
    {
        return _lineWidth;
    };

    void offset(float width)
    {
        _offset = width;
    };
    float offset()
    {
        return _offset;
    };

    bool Dessinable()
    {
        return bDessinable;
    };
    void Dessinable(bool _val)
    {
        bDessinable = _val;
    };

    void Toggle()
    {
        bDessinable = !bDessinable;
    };

    void Animable(bool _val)
    {
        bAnimable = _val;
    };
    bool Animable()
    {
        return bAnimable;
    };

    const char Key()
    {
        return _activeKey;
    };
    void Key(char _key)
    {
        _activeKey = _key;
    };

    TypeTrace typeTrace()
    {
        return _typeTrace;
    };
    void typeTrace(TypeTrace _type)
    {
        _typeTrace = _type;
    };

protected:
    Tools * compositeur;

private:
    virtual void initialiseOscMethodes();
    ofVec2f * origine;
    ofVec2f * destination;
    bool bDessinable;
    bool bAnimable;
    float _lineWidth;
    uint32_t _offset;
    char _activeKey;
    TypeTrace _typeTrace;
    ofxAnimatableFloat _alpha;
};
