#pragma once

#include "Element.h"

class Tangente :
    public Element
{
public:
    Tangente(string nom, char key, float rayon = 100, float angle = 30);
    Tangente(string nom, char key, ofVec2f * _pointOrigine, float rayon, float angle = 30);
    virtual ~Tangente();
    void initialiseOscMethodes();
    void processOscCommand(const string& command, const ofxOscMessage& m);
    void draw();
    void angle(float a)
    {
        _angle = ofWrap(a, 0, 360);
    };
    float angle()
    {
        return _angle;
    };
    void rayon(float r)
    {
        _rayon = r;
    };
    float rayon()
    {
        return _rayon;
    };
private:
    float _angle;
    float _rayon;
};
