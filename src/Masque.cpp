#include "Masque.h"

Masque * Masque::singleton = NULL;

/// \brief
///
Masque::Masque() :
    ofxOscRouterNode("masque"),
    m_TLCorner(0, 0),
    m_TRCorner(ofGetWidth(), 0),
    m_BLCorner(0, ofGetHeight()),
    m_BRCorner(ofGetWidth(), ofGetHeight()),
    masqueColor(0,0,0)
{
    addOscMethod("TLCorner");
    addOscMethod("TRCorner");
    addOscMethod("BLCorner");
    addOscMethod("BRCorner");
    addOscMethod("color");
    masqueMesh.addVertex(ofVec3f(0, 0)); // 0
    masqueMesh.addVertex(GetTLCorner()); // 1
    masqueMesh.addVertex(ofVec3f(ofGetWidth(), 0)); // 2
    masqueMesh.addVertex(GetTRCorner()); // 3
    masqueMesh.addVertex(ofVec3f(ofGetWidth(), ofGetHeight())); // 4
    masqueMesh.addVertex(GetBRCorner()); // 5
    masqueMesh.addVertex(ofVec3f(0, ofGetHeight())); // 6
    masqueMesh.addVertex(GetBLCorner()); // 7

    for(int i = 0; i < 8; ++i)
    {
        masqueMesh.addColor(masqueColor);
    }

    masqueMesh.addIndex(0); // TOP
    masqueMesh.addIndex(1);
    masqueMesh.addIndex(2);
    masqueMesh.addIndex(1);
    masqueMesh.addIndex(2);
    masqueMesh.addIndex(3);
    masqueMesh.addIndex(2); // RIGHT
    masqueMesh.addIndex(3);
    masqueMesh.addIndex(4);
    masqueMesh.addIndex(3);
    masqueMesh.addIndex(4);
    masqueMesh.addIndex(5);
    masqueMesh.addIndex(4); // BOTTOM
    masqueMesh.addIndex(5);
    masqueMesh.addIndex(6);
    masqueMesh.addIndex(5);
    masqueMesh.addIndex(6);
    masqueMesh.addIndex(7);
    masqueMesh.addIndex(6); // LEFT
    masqueMesh.addIndex(7);
    masqueMesh.addIndex(0);
    masqueMesh.addIndex(7);
    masqueMesh.addIndex(0);
    masqueMesh.addIndex(1);
    masqueMesh.setMode(OF_PRIMITIVE_TRIANGLE_STRIP);
}

Masque * Masque::instance()
{
    if(!singleton)     // Only allow one instance of class to be generated.
    {
        singleton = new Masque();
    }

    return singleton;
}

Masque::~Masque()
{
    //dtor
}

/// \brief
///
/// \param command const string&
/// \param m const ofxOscMessage&
/// \return void
///
///
void Masque::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "TLCorner"))
    {
        if(validateOscSignature("([f][f])", m))
        {
            SetTLCorner(ofVec2f(getArgAsFloatUnchecked(m, 0)*ofGetWidth(), getArgAsFloatUnchecked(m, 1)*ofGetHeight()));
        }
    }
    else if(isMatch(command, "TRCorner"))
    {
        if(validateOscSignature("([f][f])", m))
        {
            SetTRCorner(ofVec2f(getArgAsFloatUnchecked(m, 0)*ofGetWidth(), getArgAsFloatUnchecked(m, 1)*ofGetHeight()));
        }
    }
    else if(isMatch(command, "BLCorner"))
    {
        if(validateOscSignature("([f][f])", m))
        {
            SetBLCorner(ofVec2f(getArgAsFloatUnchecked(m, 0)*ofGetWidth(), getArgAsFloatUnchecked(m, 1)*ofGetHeight()));
        }
    }
    else if(isMatch(command, "BRCorner"))
    {
        if(validateOscSignature("([f][f])", m))
        {
            SetBRCorner(ofVec2f(getArgAsFloatUnchecked(m, 0)*ofGetWidth(), getArgAsFloatUnchecked(m, 1)*ofGetHeight()));
        }
    }
    else if(isMatch(command, "color"))
    {
        if(validateOscSignature("([f][f][f])", m))
        {
            masqueColor.set(getArgAsFloatUnchecked(m, 0),getArgAsFloatUnchecked(m, 1),getArgAsFloatUnchecked(m, 2));
        }
    }
}

/// \brief
///
/// \return void
///
///
void Masque::update()
{
    masqueMesh.setVertex(1, GetTLCorner()); // 1
    masqueMesh.setVertex(2, ofVec3f(ofGetWidth(), 0)); // 2
    masqueMesh.setVertex(3, GetTRCorner()); // 3
    masqueMesh.setVertex(4, ofVec3f(ofGetWidth(), ofGetHeight())); // 4
    masqueMesh.setVertex(5, GetBRCorner()); // 5
    masqueMesh.setVertex(6, ofVec3f(0, ofGetHeight())); // 6
    masqueMesh.setVertex(7, GetBLCorner()); // 7
    masqueMesh.setColorForIndices(0, masqueMesh.getNumIndices()-1, masqueColor);
}

/// \brief
///
/// \return void
///
///
void Masque::draw()
{
    masqueMesh.draw();
}











