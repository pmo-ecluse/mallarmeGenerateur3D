#include "Angle.h"

Angle::Angle(string nom, char key, ofVec2f * _pointAngle, TypeTrace _type) :
    Element(nom, key),
    _pointAngle(_pointAngle)
{
    typeTrace(_type);
}

Angle::Angle(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointAngle, ofVec2f * _pointDestination, TypeTrace _type) :
    Angle(nom, key, _pointAngle, _type)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

Angle::~Angle()
{
}

void Angle::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        float angleBA(Tools::getAngle(*pointAngle(), *Origine()));
        float angleBC(Tools::getAngle(*pointAngle(), *Destination()));
        float eloignement(ofClamp(10 * lineWidth(), 15, 50));
        ofPath traceAngle;
        traceAngle.arc(*pointAngle(), eloignement, eloignement, angleBA, angleBC);
        traceAngle.setArcResolution((int) ofClamp(10 * lineWidth(), 15, 30));
        traceAngle.lineTo(*pointAngle());
        traceAngle.close();

        if(typeTrace() == TRACE_REMPLIR)
        {
            ofPushStyle();
            ofFill();
            traceAngle.setColor(ofColor(255, 255, 255, Alpha() / 2.0 * 255));
            traceAngle.draw();
            ofPopStyle();
        }

        ofDisableAlphaBlending();
        ofPopStyle();
    }
}


