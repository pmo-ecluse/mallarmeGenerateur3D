#pragma once

#include "Element.h"
#include "MultiplesTangentes.h"

class MultiplesTangentesParalleles :
    public MultiplesTangentes
{
public:
    MultiplesTangentesParalleles(string nom, char key);
    MultiplesTangentesParalleles(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination);
    virtual ~MultiplesTangentesParalleles();
    void draw();
};
