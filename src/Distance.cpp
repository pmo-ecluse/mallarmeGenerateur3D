#include "Distance.h"

Distance::Distance(string nom, char key, int _font) :
    Element(nom, key),
    _ttfText(new ofTrueTypeFont()),
    _vecteur(new Vecteur(ofxOscRouterNode::getFirstOscNodeAlias() + "-vecteur", '@'))
{
	fontSize(_font);
    initialiseOscMethodes();
}

Distance::Distance(string nom, char key, ofVec2f * _pointOrigine, ofVec2f * _pointDestination) :
    Distance(nom, key)
{
    Origine(_pointOrigine);
    Destination(_pointDestination);
}

Distance::~Distance()
{
}

void Distance::initialiseOscMethodes()
{
    addOscMethod("fontSize");
}

void Distance::processOscCommand(const string& command, const ofxOscMessage& m)
{
    if(isMatch(command, "fontSize"))
    {
        if(validateOscSignature("([if])", m))
        {
            fontSize(getArgAsIntUnchecked(m, 0));
        }
    }
    else
    {
        Element::processOscCommand(command, m);
    }
}

void Distance::draw()
{
    if(Dessinable())
    {
        ofPushStyle();
        ofEnableAlphaBlending();
        ofSetColor(255, 255, 255, Alpha() * 255);
        ofSetLineWidth(lineWidth());
        float distance(Tools::getDistance(*Origine(), *Destination()));
        int yPos(ofGetHeight() - 30);
        ofVec2f origine(Origine()->x, yPos);
        ofVec2f destination(Destination()->x, yPos);
        ofLine(*Origine(), origine);
        ofLine(*Destination(), destination);
        _vecteur->Dessinable(Dessinable());
        _vecteur->Alpha(Alpha());
        _vecteur->lineWidth(lineWidth());
        _vecteur->Origine(&origine);
        _vecteur->Destination(&destination);
        _vecteur->draw();
        _ttfText->drawString("d=" + ofToString(distance), Origine()->x + (Destination()->x - Origine()->x) / 2, yPos - 20);
        ofDisableAlphaBlending();
        ofPopStyle();
    }
}

